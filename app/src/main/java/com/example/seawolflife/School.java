package com.example.seawolflife;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import android.support.v7.app.AppCompatActivity;

//import com.google.android.gms.appindexing.Action;
//import com.google.android.gms.appindexing.AppIndex;
//import com.google.android.gms.common.api.GoogleApiClient;



/**
 * Created by Chris on 11/11/2016.
 */

public class School extends AppCompatActivity {

    private ListView mListView;

    String[] majorArray = {"AAS", "ACC", "ACH", "ADV", "AFH", "AIM", "AMR", "AMS", "ANP", "ANT",
            "ARB", "ARH", "ARS", "ASC", "AST", "ATM", "BCP", "BIO","BME", "BUS",
            "CAR", "CCS", "CDT", "CEF", "CHE", "CHI", "CIV",
            "CLL", "CLS", "CLT", "CME","CSE", "CSK", "CWL", "DAN",
            "DIA", "EBH", "ECO", "MAT", "ESE", "MEC", "ENG",
            "WRT", "HIS", "MLV", "PHY"};

    // Search EditText
    EditText inputSearch;


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    //private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_main);

        mListView = (ListView) findViewById(R.id.school_main_list);
        ArrayAdapter adapter;

        String[] majorItems;

        if (majorArray.length == 0){
            majorItems = new String[1];
            majorItems[0] = getString(R.string.whoops);
            adapter = new ArrayAdapter(this, R.layout.nothinglistview, majorItems);
            mListView.setAdapter(adapter);
        }
        else {
            majorItems = new String[majorArray.length];
            for (int i = 0; i < majorArray.length; i++) {
                majorItems[i] = majorArray[i];
            }
            adapter = new ArrayAdapter(this, R.layout.mylistview, majorItems);
            mListView.setAdapter(adapter);
        }




//        Intent intent = getIntent();
//        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
//        TextView textView = new TextView(this);
//        textView.setTextSize(20);
//        textView.setText(message);

//        ViewGroup layout = (ViewGroup) findViewById(R.id.activity_school_main);
//        layout.addView(textView);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
       //S client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /** Called when the user clicks the Food button */
    public void gotoNothing(View view) {
        Intent intent = new Intent(this, NothingHere.class);
//        EditText editText = (EditText) findViewById(R.id.edit_message);
//        String message = editText.getText().toString();
        //intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

}

