package com.example.seawolflife;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import android.support.v7.app.AppCompatActivity;

//import com.google.android.gms.appindexing.Action;
//import com.google.android.gms.appindexing.AppIndex;
//import com.google.android.gms.common.api.GoogleApiClient;



/**
 * Created by Chris on 11/11/2016.
 */

public class NothingHere extends AppCompatActivity {

    private ListView mListView;


    String[] foodArray = {};

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    //private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_main);

        mListView = (ListView) findViewById(R.id.school_main_list);
        String[] foodItems;
        if (foodArray.length == 0){
            foodItems = new String[1];
            foodItems[0] = getString(R.string.whoops);
        }
        else {
           foodItems = new String[foodArray.length];

            for (int i = 0; i < foodArray.length; i++) {
                foodItems[i] = foodArray[i];
            }
        }



        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.nothinglistview, foodItems);
        mListView.setAdapter(adapter);

//        Intent intent = getIntent();
//        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
//        TextView textView = new TextView(this);
//        textView.setTextSize(20);
//        textView.setText(message);

//        ViewGroup layout = (ViewGroup) findViewById(R.id.activity_school_main);
//        layout.addView(textView);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        //S client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


}

